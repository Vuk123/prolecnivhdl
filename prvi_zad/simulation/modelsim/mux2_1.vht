-- Copyright (C) 1991-2013 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- ***************************************************************************
-- This file contains a Vhdl test bench template that is freely editable to   
-- suit user's needs .Comments are provided in each section to help the user  
-- fill out necessary details.                                                
-- ***************************************************************************
-- Generated on "04/27/2018 17:45:23"
                                                            
-- Vhdl Test Bench template for design  :  mux2_1
-- 
-- Simulation tool : ModelSim-Altera (VHDL)
-- 

LIBRARY ieee;                                               
USE ieee.std_logic_1164.all;                                

ENTITY mux2_1_vhd_tst IS
END mux2_1_vhd_tst;
ARCHITECTURE mux2_1_arch OF mux2_1_vhd_tst IS
-- constants                                                 
-- signals                                                   
SIGNAL A : STD_LOGIC := '0';
SIGNAL B : STD_LOGIC := '1';
SIGNAL S : STD_LOGIC := '0';
SIGNAL Z : STD_LOGIC;
COMPONENT mux2_1
	PORT (
	A : IN STD_LOGIC;
	B : IN STD_LOGIC;
	S : IN STD_LOGIC;
	Z : OUT STD_LOGIC
	);
END COMPONENT;
BEGIN
	i1 : mux2_1
	PORT MAP (
-- list connections between master ports and signals
	A => A,
	B => B,
	S => S,
	Z => Z
	);
init : PROCESS                                               
-- variable declarations                                     
BEGIN                                                        
        -- code that executes only once                      
WAIT;                                                       
END PROCESS init;                                           
always : PROCESS                                              
-- optional sensitivity list                                  
-- (        )                                                 
-- variable declarations                                      
BEGIN                                                         
        wait for 1 ns;
	S <= '1';
	wait for 1 ns;
	B <= '0';
	wait for 1 ns;
	S <= '0';
	wait for 1 ns;
	A <= '1'; 
WAIT;                                                        
END PROCESS always;                                          
END mux2_1_arch;
