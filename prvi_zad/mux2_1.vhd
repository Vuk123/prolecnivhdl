library ieee;
use ieee.std_logic_1164.all;


entity mux2_1 is 
	
		port(
			S, A, B : in std_logic;
			Z :  out std_logic
		);
end mux2_1;

architecture rtl of mux2_1 is
	
	
	
	begin
		
	mux_proc: process (S, A, B) is
		begin
			if (S = '0') then
				Z <= A;
			else
				Z<= B;
			end if;
		end process;
			
	end rtl;
	