library ieee;
use ieee.std_logic_1164.all;


entity state_machine is
	port (
		clk: in std_logic;
		reset: in std_logic;
		s_out: out std_logic	
	);
end state_machine;

architecture rtl of state_machine is
	
	type state_type is (s0, s1, s2);
	
	signal state_reg, next_state : state_type;

	begin
		state_transition: process (clk, reset) is
		begin
			if (reset = '1') then
				state_reg <= s0;
			elsif (clk'event and clk = '1') then
				state_reg <= next_state;
			end if;
		end process;
	
	next_state_logic: process (state_reg) is
	begin
		case (state_reg) is
			when s0 =>
				next_state <= s1;
			when s1 =>
				next_state <= s2;
			when s2 =>
				next_state <= s0;
		end case;
	end process;
	
	
	output_logic: process (state_reg) is
	begin
		case (state_reg) is
			when s2 =>
				s_out <= '1';
			when others	=>
				s_out <= '0';
		end case;
	end process;
	
	end;
	