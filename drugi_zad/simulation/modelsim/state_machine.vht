-- Copyright (C) 1991-2013 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- ***************************************************************************
-- This file contains a Vhdl test bench template that is freely editable to   
-- suit user's needs .Comments are provided in each section to help the user  
-- fill out necessary details.                                                
-- ***************************************************************************
-- Generated on "04/27/2018 18:03:23"
                                                            
-- Vhdl Test Bench template for design  :  state_machine
-- 
-- Simulation tool : ModelSim-Altera (VHDL)
-- 

LIBRARY ieee;                                               
USE ieee.std_logic_1164.all;                                

ENTITY state_machine_vhd_tst IS
END state_machine_vhd_tst;
ARCHITECTURE state_machine_arch OF state_machine_vhd_tst IS
-- constants 
constant clk_period: time := 20 ns;                                                                                                 
-- signals                                                   
SIGNAL clk : STD_LOGIC;
SIGNAL reset : STD_LOGIC;
SIGNAL s_out : STD_LOGIC;
COMPONENT state_machine
	PORT (
	clk : IN STD_LOGIC;
	reset : IN STD_LOGIC;
	s_out : OUT STD_LOGIC
	);
END COMPONENT;
BEGIN
	i1 : state_machine
	PORT MAP (
-- list connections between master ports and signals
	clk => clk,
	reset => reset,
	s_out => s_out
	);
clk_process : PROCESS                                               
-- variable declarations                                     
BEGIN                                                        
	clk <= '0';
	wait for clk_period/2;
	clk <= '1';
	wait for clk_period/2;                                                       
END PROCESS clk_process;         

                                  
always : PROCESS                                              
-- optional sensitivity list                                  
-- (        )                                                 
-- variable declarations                                      
BEGIN                                                         
        reset <= '1';
	wait for 3*clk_period;
	reset <= '0';
	wait for 10*clk_period;
	reset <= '1';

WAIT;                                                        
END PROCESS always;                                          
END state_machine_arch;
