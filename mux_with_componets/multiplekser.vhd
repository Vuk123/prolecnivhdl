library ieee;
use ieee.std_logic_1164.all;

entity multiplekser is
	port
	(
		-- Input ports
		a0, a1: in  std_logic;
		sel: in  std_logic;
		-- Output ports
		y	: out std_logic
	);
end multiplekser;

architecture mux_structural of multiplekser is
	component not1 is
		port
		(
			input	: in  std_logic;
			output	: out std_logic
		);
	end component;
	component and_2 is
		port
		(
			input1, input2	: in  std_logic;
			output	: out std_logic
		);
	end component;
	component or_2 is
		port
		(
			input1, input2	: in  std_logic;
			output	: out std_logic
		);
	end component;
	signal nsel: std_logic;
	signal a0_and_nsel: std_logic;
	signal a1_and_sel: std_logic;
begin
	inv_inst: not1 port map (sel, nsel);
	and2_inst1: and_2 port map (a0, nsel, a0_and_nsel);
	and2_inst2: and_2 port map (a1, sel, a1_and_sel);
	or2_inst: or_2 port map (a0_and_nsel, a1_and_sel, y);
end mux_structural;

--architecture mux_behav of multiplekser is
--begin
--	mux_proc: process (a0, a1, sel) is
--	begin
--		if (sel = '0') then
--			y <= a0;
--		else
--			y <= a1;
--		end if;
--end mux_behav;