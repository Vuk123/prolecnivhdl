library ieee;
use ieee.std_logic_1164.all;

entity and_2 is
	port
	(
		-- Input ports
		input1, input2	: in  std_logic;
		
		-- Output ports
		output	: out std_logic
	);
end and_2;

architecture and2_behav of and_2 is
begin

	output <= input1 and input2;

end and2_behav;