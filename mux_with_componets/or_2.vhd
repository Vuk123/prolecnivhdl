library ieee;
use ieee.std_logic_1164.all;

entity or_2 is
	port
	(
		-- Input ports
		input1, input2	: in  std_logic;
		
		-- Output ports
		output	: out std_logic
	);
end or_2;

architecture or2_behav of or_2 is
begin
	output <= input1 or input2;
end or2_behav;