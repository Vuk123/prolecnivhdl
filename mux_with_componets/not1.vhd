library ieee;
use ieee.std_logic_1164.all;

entity not1 is
	port
	(
		-- Input ports
		input	: in  std_logic;
		
		-- Output ports
		output	: out std_logic
	);
end not1;

architecture not1_behav of not1 is
begin

	output <= not input;

end not1_behav;
