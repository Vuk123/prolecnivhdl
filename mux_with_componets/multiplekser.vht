-- Copyright (C) 1991-2015 Altera Corporation. All rights reserved.
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, the Altera Quartus II License Agreement,
-- the Altera MegaCore Function License Agreement, or other 
-- applicable license agreement, including, without limitation, 
-- that your use is for the sole purpose of programming logic 
-- devices manufactured by Altera and sold by Altera or its 
-- authorized distributors.  Please refer to the applicable 
-- agreement for further details.

-- ***************************************************************************
-- This file contains a Vhdl test bench template that is freely editable to   
-- suit user's needs .Comments are provided in each section to help the user  
-- fill out necessary details.                                                
-- ***************************************************************************
-- Generated on "10/14/2016 19:31:48"
                                                            
-- Vhdl Test Bench template for design  :  multiplekser
-- 
-- Simulation tool : ModelSim-Altera (VHDL)
-- 

LIBRARY ieee;                                               
USE ieee.std_logic_1164.all;                                

ENTITY multiplekser_vhd_tst IS
END multiplekser_vhd_tst;
ARCHITECTURE multiplekser_arch OF multiplekser_vhd_tst IS
-- constants                                                 
-- signals                                                   
SIGNAL a0 : STD_LOGIC := '0';
SIGNAL a1 : STD_LOGIC := '1';
SIGNAL sel : STD_LOGIC := '0';
SIGNAL y : STD_LOGIC;
COMPONENT multiplekser
	PORT (
	a0 : IN STD_LOGIC;
	a1 : IN STD_LOGIC;
	sel : IN STD_LOGIC;
	y : OUT STD_LOGIC
	);
END COMPONENT;
BEGIN
	i1 : multiplekser
	PORT MAP (
-- list connections between master ports and signals
	a0 => a0,
	a1 => a1,
	sel => sel,
	y => y
	);
                                        
always : PROCESS                                              
-- optional sensitivity list                                  
-- (        )                                                 
-- variable declarations                                      
BEGIN                                                         
     	wait for 1 ns;
	sel <= '1';
	wait for 1 ns;
	a1 <= '0';
	wait for 1 ns;
	sel <= '0';
	wait for 1 ns;
	a0 <= '1';
WAIT;                                                        
END PROCESS always;                                          
END multiplekser_arch;
